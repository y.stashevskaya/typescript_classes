enum VehicleStatus {
    STOP = 'STOP',
    WORK = 'WORK',
}

interface FuelConsumptionInterface {
    start: number,
    work: number,
}

interface BaseVehicleInterface {
    readonly modelName: string;
    readonly vendorName: string;
    readonly fuel: number;

    startEngine(): void;

    stopEngine(): void;

    getEngineStatus(): VehicleStatus;

    refuel(): void;
}

interface CarInterface {
    drive(): void;
    park(): void;
}

class BaseVehicle implements BaseVehicleInterface {
    modelName: string;
    vendorName: string;
    tankCapacity: number;
    fuelConsumption: FuelConsumptionInterface;

    constructor(
        modelName: string,
        vendorName: string,
        tankCapacity: number,
        fuelConsumption: FuelConsumptionInterface,
    ) {
        this.modelName = modelName;
        this.vendorName = vendorName;
        this.tankCapacity = tankCapacity;
        this.fuelConsumption = fuelConsumption;
    }

    fuel: number = 0;
    engineStatus: VehicleStatus = VehicleStatus.STOP;

    startEngine(): void {
        this.engineStatus = VehicleStatus.WORK;
        this.fuel -= this.fuelConsumption.start;
        setTimeout(() => {
            if (this.getEngineStatus() === VehicleStatus.WORK) {
                this.fuel -= this.fuelConsumption.work;
            }
        }, 1000);
    }
    stopEngine(): void {
        this.engineStatus = VehicleStatus.STOP;
    }
    getEngineStatus(): VehicleStatus {
        return this.engineStatus;
    }
    refuel(): void {
        this.fuel = this.tankCapacity;
    }
}

class Car extends BaseVehicle implements CarInterface {
    constructor(modelName: string, vendorName: string) {
        super(modelName, vendorName, 40, { start: 3, work: 1 });
    }

    drive(): void {
        this.startEngine();
    }

    park(): void {
        this.stopEngine();
    }
}


/*
* Task 2: Counter singleton
*/
class Counter {
    static shared: Counter = null;
    count: number = 0;
    static getInstance(): Counter {
        if (Counter.shared === null) {
            Counter.shared = new Counter();
        }

        return Counter.shared;
    }

    static destroy(): void {
        Counter.shared = null;
    }

    increase(): void {
        this.count += 1;
    }

    decrease(): void {
        this.count -= 1;
    }

    getState(): number {
        return this.count
    }
}

export {BaseVehicle, Car, Counter};
